package com.dubilok.application;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "ACb1d13ac454cf6b5eb63c9e5fcbc0df6b";
    public static final String AUTH_TOKEN = "8609806387430a1459f437902f81f9d0";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380633674167"),
                new PhoneNumber("+19046472124"), str).create();
    }
}

